console.log('Hello World');

// An array in programming is simplu a list of data.

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// Arrays
/*
	- Arrays are used to store multiple values in a single variable
	- They are declared using square bracket ([]) also known as 'Array Literals'

	Syntax

	let/const arrayName = [elementA, elementB, elementC....];

*/

// Common examples of arrays

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

console.log(grades); //[98.5, 94.3, 89.2, 90.1]
console.log(computerBrands); //['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

// Possible use of an array but it is not recommended

let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(mixedArr); //[12, 'Asus', null, undefined, {}]

// Alternatve way to write arrays
let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];
console.log(myTasks); //['drink html', 'eat javascript', 'inhale css', 'bake sass']

/*
	Mini activity #1:
	Create a variable named tasks which will store an array at least 5 of your daily routine or tasks.
	Create a variable named capitalCities which will store an array at least 4 capital cities in the world.

*/

	let tasks = ['sleep', 'eat', 'work', 'code', 'drink'];
	let capitalCities = ['Tokyo', 'Phnom Penh', 'Ho Chi Minh', 'Taipei'];

	console.log(tasks); //['sleep', 'eat', 'work', 'code', 'drink']
	console.log(capitalCities); //['Tokyo', 'Phnom Penh', 'Ho Chi Minh', 'Taipei']

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];
console.log(cities); //['Tokyo', 'Manila', 'Jakarta']

// Length property
// The .length property allows us to get the total number of an items in an array.

console.log(myTasks.length); //4
console.log(cities.length); //3

let blankArr = [];
console.log(blankArr.length); //0

// length property can also be used with strings
// some array methods and properties can also be used with strings

let fullName = 'Jamie Noble';
console.log(fullName.length); //11

// length property can also set the total number of items in array, meaning we can actually DELETE the LAST ITEM in the array or shorten the array by simply updating the length property of an array.
myTasks.length = myTasks.length - 1;
console.log(myTasks.length); //3
console.log(myTasks); //['drink html', 'eat javascript', 'inhale css']

// another example using decrementation
cities.length--;
console.log(cities); //['Tokyo', 'Manila']

fullName.length = fullName.length - 1;
console.log(fullName.length); //11

fullName.length--;
console.log(fullName); //Jamie Noble

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
// theBeatles.length++;

theBeatles[4] = 'Cardo';
theBeatles[theBeatles.length] = 'Ely';
theBeatles[theBeatles.length] = 'Chito';
theBeatles[theBeatles.length] = 'MJ';
console.log(theBeatles); //['John', 'Paul', 'Ringo', 'George', 'Cardo', 'Ely', 'Chito', 'MJ']


/*
	Mini-Activity #2:
	-Create a function called addTrainers that can receive a single argument
	-add the argument in the end of theTrainers array
	-invoke and add an argument to be passed in the function
	-log theTrainers

	Trainers to be added: Brock, Misty

*/
	let theTrainers = ['Ash'];

	function addTrainers(trainer) {
		theTrainers[theTrainers.length] = trainer;
	}

	addTrainers('Brock');
	addTrainers('Misty');
	console.log(theTrainers); //['Ash', 'Brock', 'Misty']


// Reading from Arrays
/*
	-Accessing array elements is one of the more common tasks that we do with an array
	-This can be done through the use of array indexes
	-Each element in array is associated with its own index/number

	-the reason an array starts with 0 is due to how the language is designed

	Syntax
		arrayName[index];

*/

console.log(grades[0]); //98.5
console.log(computerBrands[3]); //Neo
// accessing an array element that does not exist will return 'Undefined'
console.log(grades[20]); //undefined

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]); // Shaq
console.log(lakersLegends[3]); //Magic

let currentLaker = lakersLegends[2];
console.log(currentLaker); //Lebron

console.log('Array before reassignemnts');
console.log(lakersLegends); // ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
lakersLegends[2] = 'Pau Gasol';
console.log('Array after reassignemnts');
console.log(lakersLegends);//['Kobe', 'Shaq', 'Pau Gasol', 'Magic', 'Kareem'];


/*
	Mini activity #3:
	-Create a function named findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
		-function should be able to receive one argument
		-return the blackMamba accessed by the index
		-creates a variable outside the function called blackMamba and store the value returned by the function in it
		-log the blackMamba variable in the console.
*/
	
	function findBlackMamba(index) {
		return lakersLegends[index];
	}
	let blackMamba = findBlackMamba(0);
	console.log(blackMamba);

/*
	Mini-activity #4:
	-Update and reassign the last two items in the array with your own favorite food
	-Log the updated array in your console

*/

	let favoriteFoods = [

	'Tonkatsu',
	'Adobo',
	'Hamburger',
	'Sinigang',
	'Pizza'
	];
	console.log('Array before reassignemnts');
	console.log(favoriteFoods);

	favoriteFoods[3] = 'Pasta';
	favoriteFoods[4] = 'Salad'
	console.log('Array after reassignemnts');
	console.log(favoriteFoods);

// Accessing the last elements of an array

let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length - 1;
console.log(lastElementIndex); // 4
console.log(bullsLegends.length); // 5
	
console.log(bullsLegends[lastElementIndex]); // Kukoc
console.log(bullsLegends[bullsLegends.length-1]); //Kukoc

// Adding Items into the array
let newArr = [];
console.log(newArr[0]); //undefined

newArr[0] = 'Cloud Strife';
console.log(newArr); //['Cloud Strife']

console.log(newArr[1]); //undefined
newArr[1] = 'Tifa Lockhart';
console.log(newArr); //['Cloud Strife', 'Tifa Lockhart']

newArr[newArr.length-1] = 'Aerith Gainsborough';

newArr[newArr.length] = 'Barret Wallace';
console.log(newArr); //['Cloud Strife', 'Tifa Lockhart', 'Barret Wallace']

// Looping over an Array

for (let index = 0; index < newArr.length; index++) {
	console.log(newArr[index]);
	// Cloud Strife 
	//Tifa Lockhart 
	//Barret Wallace 
}

let numArr = [5,12,30,46,40];
for(let index = 0; index < numArr.length; index++) {

	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + ' is divisible by 5');
	} else {
		console.log(numArr[index] + ' is not divisible by 5');
	}
}

	/*	
		5 is divisible by 5
		12 is not divisible by 5
		30 is divisible by 5
		46 is not divisible by 5
		40 is divisible by 5
	*/

// Multidimensional Arrays
/*
	- Multidimensional Arrays are useful for storing complex data structures
	- a practical application of this is to help visualize/create real world objects
*/

let chessBoard = [

	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
]
console.log(chessBoard);
console.log(chessBoard[1][4]); // e2
console.log('Pawn moves to: ' + chessBoard[1][5]); //f2

/*
	Mini-activity #5:

*/
console.log('Pawn moves to: ' + chessBoard[7][0]); //a8
console.log('Pawn moves to: ' + chessBoard[5][7]); //h6